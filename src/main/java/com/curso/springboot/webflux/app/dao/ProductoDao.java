package com.curso.springboot.webflux.app.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.curso.springboot.webflux.app.document.Producto;

public interface ProductoDao extends ReactiveMongoRepository<Producto, String>{

}
