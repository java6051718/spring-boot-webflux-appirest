package com.curso.springboot.webflux.app.controller;

import java.io.File;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.support.WebExchangeBindException;

import com.curso.springboot.webflux.app.document.Producto;
import com.curso.springboot.webflux.app.service.ProductoService;

import jakarta.validation.Valid;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;


@RestController
@RequestMapping("/api")
public class ProductoController {
	
	@Autowired
	private ProductoService productoService;
	
	@Value("${config.uploads.path}")
	private String path; 
	
	@GetMapping("/v1/productos")
	public Mono<ResponseEntity<Flux<Producto>>> listar() {
		return Mono.just(ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(productoService.findAll())
				);
	}
	
	@GetMapping("/v1/productos/{id}")
	public Mono<ResponseEntity<Producto>> listarByID(@PathVariable String id){
		return productoService.findById(id).map(producto -> ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(producto))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}
	

	@PostMapping("/v1/productos")
	public Mono<ResponseEntity<Map<String,Object>>> create(@Valid @RequestBody Mono<Producto> monoProductoCreate){
		
		Map<String,Object> respuesta = new HashMap<String,Object>();
		
		return monoProductoCreate.flatMap(productoCreate -> {
			
			if(productoCreate.getCreateAt()==null) {
				productoCreate.setCreateAt(new Date());
			}
			
			return productoService.save(productoCreate).map(producto -> {
					respuesta.put("producto", producto);
					respuesta.put("mensaje", "Producto creado con éxito");
					respuesta.put("timestamp", new Date());
					return ResponseEntity.created(URI.create("/api/v1/productos/".concat(producto.getId())))
						.contentType(MediaType.APPLICATION_JSON)
						.body(respuesta);
					});
			
		}).onErrorResume(t ->{
			return Mono.just(t).cast(WebExchangeBindException.class)
					.flatMap(e -> Mono.just(e.getFieldErrors()))
					.flatMapMany(errors -> Flux.fromIterable(errors))
					.map(fieldError -> "El campo "+fieldError.getField()+" "+fieldError.getDefaultMessage())
					.collectList()
					.flatMap(list -> {
						respuesta.put("errors", list);
						respuesta.put("status", HttpStatus.BAD_REQUEST.value());
						respuesta.put("timestamp", new Date());
						return Mono.just(ResponseEntity.badRequest().body(respuesta));
					});
		});
	}
	
	@PostMapping("/v2/productos")
	public Mono<ResponseEntity<Producto>> createConImagen(Producto productoCreate, @RequestPart FilePart file){
		
		if(productoCreate.getCreateAt()==null) {
			productoCreate.setCreateAt(new Date());
		}
		
		productoCreate.setFoto(UUID.randomUUID().toString()+ "-"+file.filename()
		.replace(" ", "")
		.replace(":", "")
		.replace("\\", ""));
		
		return file.transferTo(new File(path+productoCreate.getFoto())).then(productoService.save(productoCreate))
				.map(producto -> ResponseEntity
						.created(URI.create("/api/v1/productos/".concat(producto.getId())))
						.contentType(MediaType.APPLICATION_JSON)
						.body(producto)
				);
	}
	
	@PutMapping("/v1/productos/{id}")
	public Mono<ResponseEntity<Producto>> update(@PathVariable("id") String id,@RequestBody Producto productoUpdate){
		
		return productoService.findById(id).flatMap(producto -> {
			
			producto.setNombre(productoUpdate.getNombre());
			producto.setPrecio(productoUpdate.getPrecio());
			producto.setCategoria(productoUpdate.getCategoria());
			producto.setFoto(productoUpdate.getFoto());
			
			return productoService.save(producto);
			
		}).map(producto -> ResponseEntity.created(URI.create("/api/v1/productos/".concat(producto.getId())))
				.contentType(MediaType.APPLICATION_JSON)
				.body(producto))
		  .defaultIfEmpty(ResponseEntity.notFound().build());
	}
	
	@DeleteMapping("/v1/productos/{id}")
	public Mono<ResponseEntity<Void>> delete(@PathVariable("id") String id){
		return productoService.findById(id).flatMap(producto -> {
			return productoService.delete(producto).then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
		}).defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
	}
	
	@PostMapping("/v1/productos/upload/{id}")
	public Mono<ResponseEntity<Producto>> uploadImage(@PathVariable("id") String id, @RequestPart FilePart file){
		return productoService.findById(id).flatMap(producto -> {
			
			producto.setFoto(UUID.randomUUID().toString()+ "-"+file.filename()
			.replace(" ", "")
			.replace(":", "")
			.replace("\\", ""));
			
			return file.transferTo(new File(path+producto.getFoto())).then(productoService.save(producto));
			
		}).map(producto -> ResponseEntity.ok(producto))
		  .defaultIfEmpty(ResponseEntity.notFound().build());
	}
}
