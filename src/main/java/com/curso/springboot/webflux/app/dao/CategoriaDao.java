package com.curso.springboot.webflux.app.dao;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.curso.springboot.webflux.app.document.Categoria;

public interface CategoriaDao extends ReactiveMongoRepository<Categoria, String>{

}
