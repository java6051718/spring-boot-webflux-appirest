package com.curso.springboot.webflux.app.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Document(collection = "categorias")
public class Categoria {
	
	@Id
	@NotEmpty
	private String id;
	
	@NonNull
	private String nombre;

}
