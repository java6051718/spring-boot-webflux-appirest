package com.curso.springboot.webflux.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;
import com.curso.springboot.webflux.app.handler.ProductoHandler;
import static org.springframework.web.reactive.function.server.RequestPredicates.*;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class RouterFunctionConfig {
	
	private String requestMapping = "/api/v3/productos";
	
	@Bean 
	RouterFunction<ServerResponse> routes(ProductoHandler handler){
		return route(GET(requestMapping), handler::listar)
				.andRoute(GET(requestMapping+"/{id}"), handler::listarByID)
				.andRoute(POST(requestMapping), handler::create)
				.andRoute(PUT(requestMapping), handler::update);
	}
}