package com.curso.springboot.webflux.app.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import static org.springframework.web.reactive.function.BodyInserters.*;
import java.net.URI;
import java.util.Date;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import com.curso.springboot.webflux.app.document.Producto;
import com.curso.springboot.webflux.app.service.ProductoService;
import reactor.core.publisher.Mono;

@Component
public class ProductoHandler {
	
	@Autowired
	private ProductoService service;
	
	public Mono<ServerResponse> listar(ServerRequest request) {
		return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
				.body(service.findAll(), Producto.class);
	}
	
	public Mono<ServerResponse> listarByID(ServerRequest request){
		
		String id = request.pathVariable("id");
		
		return service.findById(id).flatMap(producto -> ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(fromValue(producto)))
				.switchIfEmpty(ServerResponse.notFound().build());
	}
	
	public Mono<ServerResponse> create(ServerRequest request){
		
		Mono<Producto> MonoProducto = request.bodyToMono(Producto.class);
				
		return MonoProducto.flatMap(productoCreate -> {
			
			if(productoCreate.getCreateAt()==null) {
				productoCreate.setCreateAt(new Date());
			}
			
			return service.save(productoCreate);
			
		}).flatMap(producto -> ServerResponse.created(URI.create("/api/v1/productos/".concat(producto.getId())))
				.contentType(MediaType.APPLICATION_JSON)
				.body(fromValue(producto)));
	}
	
	public Mono<ServerResponse> update(ServerRequest request){
		
		Mono<Producto> productoRequest = request.bodyToMono(Producto.class);
		
		String id = request.pathVariable("id");
		
		Mono<Producto> productoDb =  service.findById(id);
		
//		return productoDb.zipWith(productoRequest, (db, req) -> {
//			
//			db.setNombre(req.getNombre());
//			db.setPrecio(req.getPrecio());
//			db.setCategoria(req.getCategoria());
//			db.setFoto(req.getFoto());
//		});
		
//		return productoRequest.flatMap(productoUpdate -> {
//			
//			return service.findById(productoUpdate.getId()).flatMap(productoDB -> {
//				
//				productoDB.setNombre(productoUpdate.getNombre());
//				productoDB.setPrecio(productoUpdate.getPrecio());
//				productoDB.setCategoria(productoUpdate.getCategoria());
//				productoDB.setFoto(productoUpdate.getFoto());
//				
//				return service.save(productoDB);
//			});
//			
//		}).flatMap(producto -> ServerResponse.created(URI.create("/api/v1/productos/".concat(producto.getId())))
//				.contentType(MediaType.APPLICATION_JSON)
//				.body(fromValue(producto)));
		
		return null;
	}
}